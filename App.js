import React, {useState} from 'react';
import { View, Text, TextInput, Switch} from 'react-native';
import WelcomeScreen from './app/screens/WelcomeScreen';
import ViewImageScreen from './app/screens/ViewImageScreen';
import AppButton from './app/components/AppButton';
import Card from './app/components/Card';
import ListingDetailsScreen from './app/screens/ListingDetailsScreen';
import MessagesScreen from './app/screens/MessagesScreen';
import Screen from './app/components/Screen';
import Icon from './app/components/Icon';
import Listitem from './app/components/Listitem';
import AccountScreen from './app/screens/AccountScreen';
import ListingScreen from './app/screens/ListingScreen';
import AppTextInput from './app/components/AppTextInput';
import AppPicker from './app/components/AppPicker';
import AppText from './app/components/AppText';
import LoginScreen from './app/screens/LoginScreen';
import ListingEditScreen from './app/screens/ListingEditScreen';

export default function App() {
  
   
  return (
  //  <WelcomeScreen/>
  // <ViewImageScreen/>
    // <ListingDetailsScreen/>
    // <MessagesScreen/>
  //  <AccountScreen/>
  // <ListingScreen/>
  // <LoginScreen/>
  <ListingEditScreen/>
  
  );
}
