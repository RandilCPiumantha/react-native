import React from 'react';
import {useFormikContext} from 'formik';

import ErrorMessage from '../components/ErrorMessage';
import AppPicker from './AppPicker';

function AppFormPicker({items, name , placeholder, width,numberOfColumns, PickerItemComponent}) {

    const {errors, setFieldValue, touched, values} = useFormikContext();
    return (
            <>
                <AppPicker
                    items = {items}
                    onSelectItem = {(item) => setFieldValue(name, item)}
                    placeholder = {placeholder}
                    selectedItem = {values[name]}
                    width = {width}
                    PickerItemComponent = {PickerItemComponent}
                    numberOfColumns = {numberOfColumns}
                />
                 <ErrorMessage error = {errors[name]} visible = {touched[name]}/>
            </>
    );
}

export default AppFormPicker;