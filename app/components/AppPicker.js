import React, {useState} from 'react';
import { View, StyleSheet, Button, Modal, TouchableWithoutFeedback, FlatList} from 'react-native';
import {MaterialCommunityIcons} from '@expo/vector-icons';

import defaultStyle from '../config/styles';
import AppText from './AppText';
import Screen from './Screen';
import PickerItem from './PickerItem';

function AppPicker({icon, items, selectedItem, placeholder,PickerItemComponent = PickerItem ,numberOfColumns = 1, onSelectItem, width = "100%"}) {

    const [modelVisible, setmodelVisible] = useState(false);

    return (
        <>
            <TouchableWithoutFeedback onPress = {() => setmodelVisible(true)}>
                <View style = {[styles.container, {width}]}>
                    {
                    icon &&
                            <MaterialCommunityIcons 
                                name = {icon} 
                                size = {20} 
                                color = {defaultStyle.colors.medium} 
                                style = {styles.icon}
                            />
                    }
                    {selectedItem ? <AppText style = {styles.text}>{selectedItem.label}</AppText>:<AppText style = {styles.placeholder}>{placeholder}</AppText>}
                
                    <MaterialCommunityIcons 
                        name = "chevron-down" 
                        size = {20} 
                        color = {defaultStyle.colors.medium} 
                    />
                </View>
            </TouchableWithoutFeedback>
            <Modal visible = {modelVisible} animationType = "slide">
                <Screen>
                    <Button title = "close" onPress = {() => setmodelVisible(false)}/>
                    <FlatList
                        data = {items}
                        keyExtractor = {(item) => item.value.toString()}
                        numColumns = {numberOfColumns}
                        renderItem = {({item}) => 
                            <PickerItemComponent
                                item = {item}
                                label = {item.label}
                                onPress = {() =>{
                                    setmodelVisible(false);
                                    onSelectItem(item);
                                }}
                            />}
                    />
                </Screen>
            </Modal>
           
       </>
    );
}

const styles = StyleSheet.create({

    container:{
        backgroundColor:defaultStyle.colors.light,
        borderRadius:25,
        flexDirection:"row",
        padding:15,
        marginVertical:10
    },

    icon:{
        marginRight:10
    },

    placeholder:{
        color:defaultStyle.colors.medium,
        flex:1
    },

    text:{
        flex:1
    }
})

export default AppPicker;

