import React from 'react';
import { View,StyleSheet, Image, TouchableHighlight } from 'react-native';
import {MaterialCommunityIcons} from '@expo/vector-icons';

import AppText from './AppText';
import colors from '../config/color';
import Swipeable from 'react-native-gesture-handler/Swipeable';

function Listitem({title,subTitle,image,onPress,ImageComponent,renderRightActions}) {
    return (
        <Swipeable renderRightActions = {renderRightActions}>
            <TouchableHighlight
                onPress = {onPress}
                underlayColor = {colors.light}
            >
                <View style = {styles.container}>
                    {ImageComponent}
                    {image &&<Image source = {image} style = {styles.image} />}
                    <View style = {styles.detailContainer}>
                        <AppText style = {styles.title} numberOfLines = {1}>{title}</AppText>
                        {subTitle &&<AppText style = {styles.subTitle} numberOfLines = {2}>{subTitle}</AppText>}
                    </View>
                    <MaterialCommunityIcons color = {colors.medium} name = "chevron-right" size = {25}/>
                </View>
            </TouchableHighlight>
        </Swipeable>
    );
}

const styles = StyleSheet.create({
    
    container:{
        alignItems:"center",
        flexDirection:"row",
        padding:15,
        backgroundColor:colors.white
    },

    detailContainer:{
        flex:1,
        marginLeft:10,
        justifyContent:"center"

    },

    image:{
        width:70,
        height:70,
        borderRadius:35,
    },

    title:{
        fontWeight:"600"
    },

    subTitle:{
        color:colors.medium
    }

})

export default Listitem;