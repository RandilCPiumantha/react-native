import React from 'react';
import { View, StyleSheet, Image } from 'react-native';

import AppText from '../components/AppText';
import colors from '../config/color';
import ListItem from '../components/Listitem';

function ListingDetailsScreen(props) {
    return (
        <View>
            <Image style = {styles.image} source = {require("../assets/jacket.jpg")}/>
            <View style = {styles.detailsContainer}>
                <AppText style = {styles.title}>Red Jacket for Sell</AppText>
                <AppText style = {styles.price}>$100</AppText>
            </View>
            <View style = {styles.userContainer}>
                <ListItem
                    image = {require("../assets/mosh.jpg")}
                    title = "Mosh Hamedin"
                    subTitle = "5 Listings"
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
  
    image:{
        width:"100%",
        height:300
    },

    detailsContainer:{
        padding:20,
    },

    price:{

        color:colors.secondary,
        fontWeight:"bold",
        fontSize:20,
        marginVertical:10
    },

    title:{

        fontSize:24,
        fontWeight:"500",
    },

    userContainer:{
        marginVertical:20
    }

})

export default ListingDetailsScreen;