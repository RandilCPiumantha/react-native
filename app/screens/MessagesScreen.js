import React, {useState} from 'react';
import { FlatList, StyleSheet} from 'react-native';

import Listitem from '../components/Listitem';
import Screen from '../components/Screen';
import ListItemSeparator from '../components/ListItemSeparator';
import ListItemDeleteAction from '../components/ListItemDeleteAction';

const initialMessages = [
    {
        id:'1',
        title:'Mosh Hameidin',
        description:'Hello Every one',
        image:require('../assets/mosh.jpg')
    },

    {
        id:'2',
        title:'Mosh Hameidin',
        description:'My name is Mosh Hameidin',
        image:require('../assets/mosh.jpg')
    }
]


function MessagesScreen(props) {

    const [messages, setMessages] = useState(initialMessages);
    const [refreshing, setRefreshing] = useState(false);

    const handleDelete = (message) =>{

        setMessages(messages.filter((m)=> m.id !== message.id));

    };


    return (
    <Screen style = {styles.screen}>
      <FlatList
        data = {messages}
        keyExtractor = {(message) => message.id.toString()}
        renderItem = {({item}) => 
            <Listitem
                title = {item.title}
                subTitle = {item.description}
                image = {item.image}
                onPress = {() => console.log("Message Selected",item)}
                renderRightActions = {() =>
                    <ListItemDeleteAction 
                        onPress = {() => handleDelete(item)}
                    />}
            />}
        ItemSeparatorComponent = {ListItemSeparator}
        refreshing = {refreshing}
        onRefresh = {() =>{ 
        setMessages([
            {
                id:'2',
                title:'T2',
                description:'D2',
                image:require('../assets/mosh.jpg')
            }
        ])
    }}
      />
    </Screen>
    );
}


const styles = StyleSheet.create({
    

})

export default MessagesScreen;