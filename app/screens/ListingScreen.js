import React from 'react';
import { FlatList , StyleSheet } from 'react-native';

import Screen from '../components/Screen';
import Card from '../components/Card';
import color from '../config/color';

const listings = [
    {
        id:'1',
        title:'Red jacket for sale',
        price:'100',
        image:require('../assets/jacket.jpg')

    },

    {
        id:'2',
        title:'Couch in Great Condition ',
        price:'400',
        image:require('../assets/couch.jpg')

    }
]


function ListingScreen(props) {

    return (
       <Screen style = {styles.screen}>
           <FlatList
                data = {listings}
                keyExtractor = {(listing) =>listing.id.toString()}
                renderItem = {({item}) => 
                    <Card 
                        title = {item.title}
                        subTitle = {"$"+item.price}
                        image = {item.image}
                    />
                }
           />
       </Screen>
    );
}

const styles = StyleSheet.create({
    
    screen:{
        padding:20,
        backgroundColor:color.light
    }

 })

export default ListingScreen;