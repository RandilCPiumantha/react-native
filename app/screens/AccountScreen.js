import React from 'react';
import { StyleSheet, FlatList, View } from 'react-native';

import Screen from '../components/Screen';
import Listitem from '../components/Listitem';
import colors from '../config/color';
import Icon from '../components/Icon';
import ListItemSeparator from '../components/ListItemSeparator';


const menuIem = [
    {
        title:"My Listing",

        icon:{
            name:"format-list-bulleted",
            backgroundColor:colors.primary
        }
    },

    {
        title:"My Messages",

        icon:{
            name:"email",
            backgroundColor:colors.secondary
        }
    }
]


function AccountScreen(props) {
    return (
       <Screen style = {styles.screen}>
           <View style = {styles.container}>
                <Listitem
                    title = "Mosh Hamedien"
                    subTitle = "www.randil250.rp@gmail.com"
                    image = {require('../assets/mosh.jpg')}
                />
           </View>
           <View styles = {styles.container}>
                <FlatList
                    data = {menuIem}
                    keyExtractor = {(item) => item.title}
                    ItemSeparatorComponent = {ListItemSeparator}
                    renderItem = {({item})=> 
                        <Listitem
                            title = {item.title}
                            ImageComponent ={ 
                                <Icon   name = {item.icon.name}
                                        backgroundColor = {item.icon.backgroundColor}
                                />}
                        />}
                />
           </View>
           <View style = {styles.container}>
            <Listitem
                title = "Log Out"
                ImageComponent = {
                    <Icon name = "logout" backgroundColor = "#ffe66d"/>
                }
            />
           </View>
       </Screen>
    );
};

const styles = StyleSheet.create({
    container:{
        marginVertical:20,
    },

    screen:{
        backgroundColor:colors.light
    }
})


export default AccountScreen;

