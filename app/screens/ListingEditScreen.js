import React from 'react';
import { StyleSheet } from 'react-native';
import *  as Yup from 'yup';

import Screen from '../components/Screen';
import AppFormField from '../components/AppFormField';
import SubmitButton from '../components/SubmitButton';
import AppForm from '../components/AppForm';
import AppFormPicker from '../components/AppFormPicker';
import CategoryPickerItem from '../components/CategoryPickerItem';


 const validationSchema = Yup.object().shape({
     title:Yup.string().required().min(1).label("Title"),
     price: Yup.number().required().min(1).max(10000).label("Price"),
     description: Yup.string().label("Description"),
     category: Yup.object().required().nullable().label("Category")
 })


 const categories = [

     {label:"Apps", value: 1, backgroundColor:"red", icon:"apps"},
     {label:"Clotings", value: 2, backgroundColor:"#4ecdc4", icon:"shoe-heel"},
     {label:"Cameras", value: 3, backgroundColor:"green", icon:"camera"},
     {label:"Alarms", value: 4, backgroundColor:"orange", icon:"bell-ring"},
     {label:"Sports", value: 5, backgroundColor:"gray", icon:"basketball"},
     {label:"Cars", value: 6, backgroundColor:"black", icon:"car-sports"},
     {label:"Books", value: 7, backgroundColor:"#0c0c0c", icon:"book-open"},
     {label:"Wood Works", value: 8, backgroundColor:"#ff5252", icon:"baseball-bat"},
     {label:"More", value: 9, backgroundColor:"indigo", icon:"dots-horizontal-circle"},
 ]


function ListingEditScreen(props) {
    return (
        <Screen style = {styles.container}>
            <AppForm
                initialValues = {{title:'', price:'', description: ''}}
                onSubmit = {(values) => console.log(values)}
                validationSchema = {validationSchema}>
                <AppFormField 
                    maxLength = {255} 
                    name = "title" 
                    placeholder = "Title"
                />
                <AppFormField
                    keyboardType = "numeric"
                    maxLength = {8} 
                    name = "price" 
                    placeholder = "Price"
                    width = {120}
                />
                <AppFormField
                    keyboardType = "numeric"
                    maxLength = {8} 
                    name = "price" 
                    placeholder = "Price"
                />  
                <AppFormPicker
                    items = {categories}
                    name = "category"
                    placeholder = "Category"
                    width = "50%"
                    PickerItemComponent = {CategoryPickerItem}
                    numberOfColumns = {3}
                />
                <AppFormField
                    maxLength = {255}
                    multiline
                    name = "description" 
                    numberofLines = {3}
                    placeholder = "Description"
                />  

                <SubmitButton title = "Post"/>
            </AppForm>
        </Screen>
    );
}

const styles = StyleSheet.create({
    container:{
        padding:10
    }
})

export default ListingEditScreen;